# Grandma Zona Slideshow

[![pipeline status](https://gitlab.com/jrop/grandma-zona-slideshow/badges/master/pipeline.svg)](https://gitlab.com/jrop/grandma-zona-slideshow/commits/master)

This repo houses the code to generate the slideshow for Grandma Zona's funeral.

# [Obituary](https://www.carlsonfh.net/obituaries/Zona-Mary-Homeier?obId=7454511&fbclid=IwAR06n3LyhxDKemBrXGb0ipDIgU2Rk9nSavKz2Daecl9NaulXdC0FvKQWTiU#/obituaryInfo)

Zona Mary Homeier, 96, of Wilson, Kansas, passed away Monday, September 23rd, 2019.

Zona was born in Rural Osborne County, Kansas on September 1, 1923, a daughter of the late Guela Louise (Westphall) and Harry Americus McReynolds.

She had many jobs in her life, such as a telephone operator, Nurses aid at Wilson Nursing Home, EMT, Shaw's Grocery, and Kansas Originals.

Zona was a member of Immanuel Lutheran Church, Wilson, Wilson American Legion Auxiliary and a volunteer at the Wilson Senior Center.

Survivors include two daughters, LaNita Pratt (Jim), of Salina, Kansas, Susan Homeier, of Ellsworth, Kansas; three sons, Richard Homeier (Cathy), of Fort Collins, Colorado; Raymond Homeier (Mary Ann), of Wilson, Kansas; Dale Homeier (Carmen), of Wilson, Kansas; three sisters, LaVona Ruark of Sterling, Colorado, Evelyn Ray of Inman, Kansas, Lois Hickey of Inman, Kansas; and brother Wayne McReynolds, Manhattan, Kansas. She is also survived by 11 grandchildren, 23 great grandchildren, 2 great great grandchildren.

She is preceded in death by her husband, Ferdinand Erich Homeier; two sisters, Velda Brooks, Betty Duryee; three brothers, Carl McReynolds, Ralph McReynolds, Harry McReynolds Jr.; and grandson, Kevin Homeier.

The family will receive friends from 7:00 pm - 8:00 pm, Tuesday, October 1st at Immanuel Lutheran Church.

Memorial Services will be at 2:00 pm, Wednesday, October 2nd at Immanuel Lutheran Church, Wilson, Kansas, with Rev. Delvin Strecker officiating. Inurnment will follow at Wilson City Cemetery, Wilson, Kansas.

Donations may be made to Immanuel Lutheran Church or to the donor’s choice in care of Carlson-Geisendorf Funeral Home, 500 S. Ohio, Salina, Kansas 67401

To send flowers to the family of Zona Mary Homeier, please visit Tribute Store.
