const fs = require('fs');
const globby = require('globby');

const html = `
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="node_modules/reveal.js/css/reveal.css" />
  <link rel="stylesheet" href="node_modules/reveal.js/css/theme/white.css" />
</head>
<body>
  <div class="reveal">
    <div class="slides">
      ${globby
        .sync('./img/*.{jpg,png}')
        .map(
          f =>
            `<section data-background-image="${f}" data-background-size="contain">&nbsp;</section>`,
        )
        .join('\n')}
        <section data-background-video="./img/69.mp4" data-background-size="contain">&nbsp;</section>
    </div>
  </div>
  <script src="node_modules/reveal.js/js/reveal.js"></script>
  <script>
    Reveal.initialize({
      autoPlayMedia: true,
      autoSlide: 5000,
      loop: true
    });
    for (const vid of Array.from(document.querySelectorAll('video'))) {
      vid.addEventListener('ended', () => Reveal.isAutoSliding() && Reveal.next());
    }
  </script>
</body>
</html>
`;

fs.writeFileSync('./index.html', html);
