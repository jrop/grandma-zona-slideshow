const path = require('path');
const globby = require('globby');
const child_process = require('child_process');

const files = globby.sync('img/*.{jpg,JPG,png,PNG}');
let i = 0;
for (const f of files) {
  const parsedPath = path.parse(f);
  const dest = `img/${i}${parsedPath.ext.toLowerCase()}`;
  if (f !== dest) child_process.execSync(`mv "${f}" "${dest}"`);
  i++;
}
